// SPDX-License-Identifier: GPL-2.0-only

/dts-v1/;

#include "msm8939.dtsi"
#include "pm8916.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/pinctrl/qcom,pmic-mpp.h>

/ {
	model = "Xiaomi Mi 4i";
	compatible = "xiaomi,ferrari", "qcom,msm8939-mtp", "qcom,msm8939", "qcom,mtp";
	qcom,msm-id = <239 0>;
	qcom,board-id = <8 0>;

	aliases {
		serial0 = &blsp1_uart2;
	};

	chosen {
		stdout-path = "serial0";
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		ramoops: ramoops_mem_region@ffe00000 {
			compatible = "ramoops";
			reg = <0 0xffe00000 0 0x2000000>;

			record-size = <0x0>;
			console-size = <0x40000>;
			ftrace-size = <0x1000>;
		};

		cont_splash_mem: framebuffer@83000000 {
			reg = <0x0 0x83000000 0x0 0x2000000>;
			no-map;
		};
	};

	gpio-keys {
		compatible = "gpio-keys";
		input-name = "gpio-keys";
		#address-cells = <1>;
		#size-cells = <0>;
		autorepeat;

		label = "GPIO Buttons";

		button@0 {
			label = "Volume Up";
			gpios = <&msmgpio 107 GPIO_ACTIVE_LOW>;
			linux,input-type = <1>;
			linux,code = <KEY_VOLUMEUP>;
			wakeup-source;
			debounce-interval = <15>;
		};
	};

	usb_id: usb-id {
		compatible = "linux,extcon-usb-gpio";
		id-gpio = <&msmgpio 110 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&usb_id_default>;
	};
};

&blsp_dma {
	status = "okay";
};

&blsp_i2c4 {
	status = "okay";

	charger: battery@14 {
		compatible = "qcom,smb1360";
		reg = <0x14>;

		interrupt-parent = <&msmgpio>;
		interrupts = <62 IRQ_TYPE_LEVEL_LOW>;

		pinctrl-names = "default";
		pinctrl-0 = <&smb_int_default>;

		/* Set by lk2nd */
		status = "disabled";

		qcom,float-voltage-mv = <4400>;
		qcom,iterm-ma = <150>;
		qcom,charging-timeout = <192>; /* Not configured in downstream */
		qcom,recharge-thresh-mv = <100>;
		qcom,chg-inhibit-disabled;

		/* battery-profile will be set by lk2nd */
		// qcom,battery-profile = <0>; /* downstream uses qcom,batt-profile-select; */
		qcom,fg-cutoff-voltage-mv = <3450>;
		qcom,thermistor-c1-coeff = <0x85dd>;
		qcom,fg-auto-recharge-soc = <95>; /* 99 in downstream */

		qcom,rsense-10mohm;

		qcom,otp-hard-jeita-config;
		qcom,otp-hot-bat-decidegc = <600>;
		qcom,otp-cold-bat-decidegc = <0>;

		qcom,soft-jeita-config;
		qcom,warm-bat-decidegc = <450>;
		qcom,cool-bat-decidegc = <100>; /* 0 in downstream */
		qcom,soft-jeita-comp-voltage-mv = <4100>;
		qcom,soft-jeita-comp-current-ma = <750>; /* 200 in downstream */

		qcom,shdn-after-pwroff;
		qcom,fg-reset-at-pon;

		usb_otg_vbus: usb-otg-vbus {
			regulator-max-microamp = <1500000>;
		};
	};
};

&blsp_i2c5 {
	status = "okay";
	touchscreen@4a {
		status = "disabled"; /* disabled because ghost touch issues */
		compatible = "atmel,maxtouch";
		reg = <0x4a>;

		interrupt-parent = <&msmgpio>;
		interrupts = <13 IRQ_TYPE_LEVEL_LOW>;

		reset-gpio = <&msmgpio 12 GPIO_ACTIVE_LOW>;

		vdd-supply = <&pm8916_l6>;

		pinctrl-names = "default", "sleep";
		pinctrl-0 = <&ts_int_active &ts_reset_active &ts_pwr_active>;
		pinctrl-1 = <&ts_int_suspend &ts_reset_suspend &ts_pwr_suspend>;
	};
};

&msmgpio {

	ts_pins: ts_pins {
		ts_int_active: pmx_ts_int_active {
				pins = "gpio13";
				function = "gpio";

				drive-strength = <16>;
				bias-pull-up;
		};

		ts_int_suspend: pmx_ts_int_suspend {
				pins = "gpio13";
				function = "gpio";

				drive-strength = <2>;
				bias-pull-down;
		};

		ts_reset_active: pmx_ts_reset_active {
				pins = "gpio12";
				function = "gpio";

				drive-strength = <16>;
				bias-pull-up;
		};

		ts_reset_suspend: pmx_ts_reset_suspend {
				pins = "gpio12";
				function = "gpio";

				drive-strength = <2>;
				bias-pull-down;
		};

		ts_pwr_active: pmx_ts_pwr_active {
				pins = "gpio9", "gpio78";
				function = "gpio";

				drive-strength = <16>;
				bias-pull-up;
		};

		ts_pwr_suspend: pmx_ts_pwr_suspend {
				pins = "gpio9", "gpio78";
				function = "gpio";

				drive-strength = <2>;
				bias-disable;
		};
	};

	pmx_mdss {
		mdss_default: mdss_default {
			pins = "gpio25";
			function = "gpio";

			drive-strength = <8>;
			bias-disable;
		};

		mdss_sleep: mdss_sleep {
			pins = "gpio25";
			function = "gpio";

			drive-strength = <2>;
			bias-pull-down;
		};
	};

	usb_id_default: usb_id_default {
		pins = "gpio110";
		function = "gpio";

		drive-strength = <8>;
		bias-pull-up;
	};

	smb_int_default: smb_int_default {
		pins = "gpio62";
		function = "gpio";

		drive-strength = <2>;
		bias-pull-up;
	};
};

&rpm_requests {
	smd_rpm_regulators: pm8916-regulators {
		compatible = "qcom,rpm-pm8916-regulators";
		vdd_l1_l2_l3-supply = <&pm8916_s3>;
		vdd_l4_l5_l6-supply = <&pm8916_s4>;
		vdd_l7-supply = <&pm8916_s4>;

		/* s1 s2 and l3 are based on power domain model */

		pm8916_s3: s3 {
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <1300000>;
		};

		pm8916_s4: s4 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <2100000>;
		};

		/* l1 is fixed to 1225000, but not connected in schematic */

		pm8916_l2: l2 {
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <1200000>;
		};

		pm8916_l4: l4 {
			regulator-min-microvolt = <2050000>;
			regulator-max-microvolt = <2050000>;
		};

		pm8916_l5: l5 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-always-on;
		};

		pm8916_l6: l6 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-always-on;
		};

		pm8916_l7: l7 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
		};

		pm8916_l8: l8 {
			regulator-min-microvolt = <2850000>;
			regulator-max-microvolt = <2900000>;
		};

		pm8916_l9: l9 {
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
		};

		pm8916_l10: l10 {
			regulator-min-microvolt = <2700000>;
			regulator-max-microvolt = <2800000>;
		};

		pm8916_l11: l11 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <2950000>;
			regulator-allow-set-load;
			regulator-system-load = <200000>;
		};

		pm8916_l12: l12 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <2950000>;
		};

		pm8916_l13: l13 {
			regulator-min-microvolt = <3075000>;
			regulator-max-microvolt = <3075000>;
		};

		pm8916_l14: l14 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
		};

		pm8916_l15: l15 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
		};

		/* v3.10 set l16 as l15, but not connected on schematic */

		pm8916_l17: l17 {
			regulator-min-microvolt = <2850000>;
			regulator-max-microvolt = <2850000>;
		};

		pm8916_l18: l18 {
			regulator-min-microvolt = <2700000>;
			regulator-max-microvolt = <2700000>;
		};
	};
};

&mdss {
	status = "okay";
};

&mdss_dsi0 {
	status = "okay";
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&mdss_default>;
	pinctrl-1 = <&mdss_sleep>;

	vdda-supply = <&pm8916_l2>;
	vddio-supply = <&pm8916_l6>;
	
	panel@0 {
		compatible = "jdi,nt35595";
		reg = <0>;

		power-supply = <&pm8916_l17>;
		reset-gpios = <&msmgpio 25 GPIO_ACTIVE_LOW>;

		ports {
			#address-cells = <1>;
			#size-cells = <0>;

			port@0 {
				reg = <0>;
				panel_in: endpoint {
					remote-endpoint = <&dsi0_out>;
				};
			};
		};
	};

	ports {
		port@1 {
			endpoint {
				remote-endpoint = <&panel_in>;
				data-lanes = <0 1 2 3>;
			};
		};
	};
};

&mdss_dsi0_phy {
	status = "okay";
	vddio-supply = <&pm8916_l6>;
	qcom,dsi-phy-regulator-ldo-mode;
};

&remoteproc_pronto {
	status = "okay";
	iris {
		compatible = "qcom,wcn3680";
	};
};

&sdhc_1 {
	status = "okay";

	vmmc-supply = <&pm8916_l8>;
	vqmmc-supply = <&pm8916_l5>;

	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&sdc1_clk_on &sdc1_cmd_on &sdc1_data_on>;
	pinctrl-1 = <&sdc1_clk_off &sdc1_cmd_off &sdc1_data_off>;
};

&sdhc_2 {
	status = "disabled";

	vmmc-supply = <&pm8916_l11>;
	vqmmc-supply = <&pm8916_l12>;

	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&sdc2_clk_on &sdc2_cmd_on &sdc2_data_on>;
	pinctrl-1 = <&sdc2_clk_off &sdc2_cmd_off &sdc2_data_off>;
};

&usb {
	status = "okay";
	extcon = <&usb_id>, <&usb_id>;
};

&usb_hs_phy {
	extcon = <&usb_id>;
	v1p8-supply = <&pm8916_l7>;
	v3p3-supply = <&pm8916_l13>;
};

&pm8916_resin {
	status = "okay";
	linux,code = <KEY_VOLUMEDOWN>;
};
